<?php

// change the following paths if necessary

if ($_SERVER["HTTP_HOST"] === 'localhost') {
    $yii=dirname(__FILE__).'/../yii/framework/yii.php';
    $config=dirname(__FILE__).'/protected/config/main.php';
    defined('YII_DEBUG');
    define('YII_TRACE_LEVEL',3);
} else {
    $yii=dirname(__FILE__).'/../../../yii/framework/yii.php';
    $config=dirname(__FILE__).'/protected/config/main.php';
}

require_once($yii);
$app = Yii::createWebApplication($config);

$app->run();
