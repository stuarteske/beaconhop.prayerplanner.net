<?php
/**
 * This is the bootstrap file for test application.
 * This file should be removed when the application is deployed for production.
 */

if ($_SERVER["HTTP_HOST"] === 'localhost') {
    $yii=dirname(__FILE__).'/../yii/framework/yii.php';
    $config=dirname(__FILE__).'/protected/config/main.php';

    defined('YII_DEBUG') or define('YII_DEBUG',true);
} else {
    die('You do not have permission to access this file');
    $yii=dirname(__FILE__).'/../../../yii/framework/yii.php';
    $config=dirname(__FILE__).'/protected/config/test.php';
}

require_once($yii);
Yii::createWebApplication($config)->run();