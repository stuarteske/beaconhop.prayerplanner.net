<?php $this->beginContent('//layouts/main'); ?>
<div id="content_body_12" class="col col_12">
	<?php echo $content; ?>
</div><!-- col_12 -->
<div id="sidebar_4"  class="col col_4">
	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Operations',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
	?>
	</div><!-- col_4 -->
</div>
<?php $this->endContent(); ?>