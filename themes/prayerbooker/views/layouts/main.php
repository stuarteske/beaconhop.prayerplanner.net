<!DOCTYPE HTML>
<html>
    <head>
    <meta charset=utf-8>
    <title>Boiler Room Booker</title>
    
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/modernizr-1.7.min.js"></script><!-- this is the javascript allowing html5 to run in older browsers -->
    <!--[if IE]><script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/excanvas.js"></script><![endif]-->
    
    <!-- latest jquery library -->
    <script src="http://c.fzilla.com/1286136086-jquery.js"></script>
    
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/reset.css" media="screen" title="html5doctor.com Reset Stylesheet" />
    
    <!-- in the CSS3 stylesheet you will find examples of some great new features CSS has to offer -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/css3.css" media="screen" />
    
    <!-- general stylesheet contains some default styles, you do not need this, but it helps you keep a uniform style -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/general.css" media="screen" />
    
    <!-- grid's will help you keep your website appealing to your users, view 52framework.com website for documentation -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/grid.css" media="screen" />
    
    <!-- special styling for forms, this can be used as a form framework on its own -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/forms.css" media="screen" />
    
    <!-- embeds necessary for html5 video player, take this section out if you are not using the video player -->
    
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/video/video.js" type="text/javascript" charset="utf-8"></script>
    
    <script type="text/javascript" charset="utf-8">
 	$(function(){
    	VideoJS.setupAllWhenReady();
    })
    </script>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/video/video-js.css" type="text/css" media="screen" charset="utf-8">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/video/vim.css" type="text/css" media="screen" charset="utf-8">
	<!-- end video embed scripts and styles -->
    
    <!-- this script is needed to accomplish html5 validation, uses jquery tools validator script -->
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/validator.js"></script>
    <script>
		$(function(){
			$("form").validator()
		})
	</script>
    
    <!-- Canvas example taken from http://www.williammalone.com/articles/html5-canvas-example/ -->
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/canvas/canvas_example.js"></script>
	
    <!-- this script is needed for using advanced css selectors in your css -->
    <!--[if (gte IE 6)&(lte IE 8)]>
    	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/selectivizr.js"></script>
    <![endif]-->  
    
    <!-- the following style is for demonstartion purposes only and is not needed for anything but inspiration -->
    <style>
    	body {
    		background:#e7e9e9;
    		background-image: linear-gradient(bottom, rgb(231,233,233) 0%, rgb(250,250,250) 100%);
			background-image: -o-linear-gradient(bottom, rgb(231,233,233) 0%, rgb(250,250,250) 100%);
			background-image: -moz-linear-gradient(bottom, rgb(231,233,233) 0%, rgb(250,250,250) 100%);
			background-image: -webkit-linear-gradient(bottom, rgb(231,233,233) 0%, rgb(250,250,250) 100%);
			background-image: -ms-linear-gradient(bottom, rgb(231,233,233) 0%, rgb(250,250,250) 100%);

			background-image: -webkit-gradient(
				linear,
				left bottom,
				left top,
				color-stop(0, rgb(231,233,233)),
				color-stop(1, rgb(250,250,250))
			);
    	}
    		
		header {padding-top:25px; border-bottom:1px solid #ccc; padding-bottom:20px;}
		header .logo {font-size:3.3em; background:transparent url('<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png') no-repeat top left; height: 56px; text-indent: 38px; color:#333;}
		header nav ul li {float:left; margin-top:12px;}
		header nav ul li a {display:block; padding:5px 15px; border-right:1px solid #eee; font-size:1.52em; font-family:Georgia, "Times New Roman", Times, serif; text-decoration:none;}
		header nav ul li a:hover {background-color:#eee; border-right:1px solid #ccc; text-shadow:-1px -1px 0px #fff;}
		header nav ul li.last a {border-right:none;}
		
		#css3 div > div {margin:0px 0px 50px 0px; padding:6px; border:1px solid #eee;}
		#grid div {text-align:center;  }
		#grid div > .col {padding:10px 0px; margin-top:10px; outline:none;}
		#grid div > .small_date {font-size:0.8em; color:#666;}
		#grid div > .gradient_1 {
			background-image: linear-gradient(bottom, rgb(231,233,233) 0%, rgb(247,249,249) 100%);
			background-image: -o-linear-gradient(bottom, rgb(231,233,233) 0%, rgb(247,249,249) 100%);
			background-image: -moz-linear-gradient(bottom, rgb(231,233,233) 0%, rgb(247,249,249) 100%);
			background-image: -webkit-linear-gradient(bottom, rgb(231,233,233) 0%, rgb(247,249,249) 100%);
			background-image: -ms-linear-gradient(bottom, rgb(231,233,233) 0%, rgb(247,249,249) 100%);

			background-image: -webkit-gradient(
				linear,
				left bottom,
				left top,
				color-stop(0, rgb(231,233,233)),
				color-stop(1, rgb(247,249,249))
			);
		}
		#grid div > .gradient_2 {
			background-image: linear-gradient(bottom, rgb(240,242,242) 0%, rgb(247,249,249) 100%);
			background-image: -o-linear-gradient(bottom, rgb(240,242,242) 0%, rgb(247,249,249) 100%);
			background-image: -moz-linear-gradient(bottom, rgb(240,242,242) 0%, rgb(247,249,249) 100%);
			background-image: -webkit-linear-gradient(bottom, rgb(240,242,242) 0%, rgb(247,249,249) 100%);
			background-image: -ms-linear-gradient(bottom, rgb(240,242,242) 0%, rgb(247,249,249) 100%);

			background-image: -webkit-gradient(
				linear,
				left bottom,
				left top,
				color-stop(0, rgb(240,242,242)),
				color-stop(1, rgb(247,249,249))
			);
			border: 1px solid #ddd;
		}
		#grid div > .clear_border {
			border:solid 1px transparent};
		}
		#grid div > .button_1 {
   			border-top: 1px solid #f7f9f9;
   			background: #e7e9e9;
   			background: -webkit-gradient(linear, left top, left bottom, from(#e7e9e9), to(#d7d9d9));
   			background: -webkit-linear-gradient(top, #e7e9e9, #d7d9d9);
   			background: -moz-linear-gradient(top, #e7e9e9, #d7d9d9);
   			background: -ms-linear-gradient(top, #e7e9e9, #d7d9d9);
   			background: -o-linear-gradient(top, #e7e9e9, #d7d9d9);
   			padding: 8px 20px;
   			-webkit-border-radius: 5px;
   			-moz-border-radius: 5px;
   			border-radius: 5px;
   			/*-webkit-box-shadow: rgba(0,0,0,.4) 0 1px 0;
   			-moz-box-shadow: rgba(0,0,0,.4) 0 1px 0;
   			box-shadow: rgba(0,0,0,.4) 0 1px 0;*/
   			text-shadow: rgba(255,255,255,.65) 0 1px 0;
   			color: #333;
   			text-decoration: none;
   			vertical-align: middle;
   		}
		#grid div > .button_1:hover {
   			border-top-color: #e7e9e9;
   			background: #e7e9e9;
   			color: #333;
   		}
		#grid div > .button_1:active {
   			border-top-color: #e7e9e9;
   			background: #e7e9e9;
   		}
		#grid div > .height_90 {
			height:90px;
		}
		#grid div > .book_1 {
			background:transparent url('<?php echo Yii::app()->theme->baseUrl; ?>/images/book_button.png') no-repeat 0px 0px;
		}
		#grid div > .book_2 {
			background:transparent url('<?php echo Yii::app()->theme->baseUrl; ?>/images/book_button.png') no-repeat -100px 0px;
		}
		#grid div > .book_3 {
			background:transparent url('<?php echo Yii::app()->theme->baseUrl; ?>/images/book_button.png') no-repeat -200px 0px;
		}
		
		.vim-css {margin:10px auto;}
		
		
		h2 {border-bottom:1px dashed #ccc; margin-top:15px;}
		
		.documentation {display:block; background-color:#eee; padding:6px 13px; font-family:Georgia, "Times New Roman", Times, serif; color:#666; text-align:right; text-shadow:-1px -1px 0px #fff;}
		
		section {
			padding:20px 0px;
			border-bottom: 1px solid #CCCCCC;
		}
		
		footer {
			padding:20px 0px;
			text-align:center;
			color:#666;
			font-size:0.9em;
		}
	</style>
    </head>

<body>
<div class="row">
	<header>
    	
    	<div class="logo col_7 col">
        	Boiler Room Booker
        </div><!-- logo col_7 -->
        
      	<nav class="col_9 col">
			<?php 
				$this->widget('zii.widgets.CMenu',array(
					'items'=>array(
						array('label'=>'Home', 'url'=>array('/site/index')),
						array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
						array('label'=>'Contact', 'url'=>array('/site/contact')),
						array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
					),
				)); 
			?>
		</nav>
		<div class="clear"></div><!-- clear -->
    </header>
</div><!-- row -->

<!-- this section contain the calandar -->
<section class="row">
	<?php echo $content; ?>
</section><!-- row -->

<div class="clear"></div><!-- clear -->

<footer class="row">

	Copyright © 2012 <a href="http://stuarteske.com/" title="Visit Stuarteske.com">Stuart Eske</a>, All Right Reserved.

</footer>
</body>
</html>
