<?php
$this->breadcrumbs=array(
	'Bookings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Update Booking #'.$model->id,'url'=>array('update','id'=>$model->id),'active'=>true),
	array('label'=>'List Bookings','url'=>array('index')),
	//array('label'=>'Create Bookings','url'=>array('create')),
	array('label'=>'View Bookings','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Bookings','url'=>array('admin')),
);
?>

<div class="page-header">
	<h1>Update Booking #<?php echo $model->id; ?> <small>Make changes to this Booking</small></h1>
</div>

<div class="">
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
</div>