<?php
$this->breadcrumbs=array(
	'Bookings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'View Booking #'.$model->id,'url'=>array('view','id'=>$model->id),'active'=>true),
	array('label'=>'List Bookings','url'=>array('index')),
	//array('label'=>'Create Bookings','url'=>array('create')),
	array('label'=>'Update Bookings','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Booking','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Bookings','url'=>array('admin')),
);
?>

<div class="page-header">
	<h1>View Booking #<?php echo $model->id; ?> <small>Details for Booking #<?php echo $model->id; ?></small></h1>
</div>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'users_name',
		'username',
		'slot_booked',
		array(
			'name'=>'slot_locked',
			'value'=>CHtml::encode($model->getLockedText())
		),
		array(
			'name'=>'slot_disabled',
			'value'=>CHtml::encode($model->getDisabledText())
		),
	),
)); ?>