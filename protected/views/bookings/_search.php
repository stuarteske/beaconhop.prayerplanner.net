<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'booked_user_id'); ?>
		<?php echo $form->textField($model,'booked_user_id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'users_name'); ?>
		<?php echo $form->textField($model,'users_name',array('size'=>60,'maxlength'=>64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'edited_user_id'); ?>
		<?php echo $form->textField($model,'edited_user_id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'time_created'); ?>
		<?php echo $form->textField($model,'time_created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'time_edited'); ?>
		<?php echo $form->textField($model,'time_edited'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'slot_booked'); ?>
		<?php echo $form->textField($model,'slot_booked'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'slot_locked'); ?>
		<?php echo $form->textField($model,'slot_locked'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'slot_disabled'); ?>
		<?php echo $form->textField($model,'slot_disabled'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->