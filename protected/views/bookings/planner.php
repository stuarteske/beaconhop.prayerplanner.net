<?php
	$invalid_flag = false;
	if ( (isset($_GET['wid'])) ) {
		if (is_numeric($_GET['wid'])) {
			$wid = filter_input(INPUT_GET, 'wid', FILTER_VALIDATE_INT);
		} else {
			$invalid_flag = true;
		}
	}
	if ($invalid_flag) throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
?>

<section id="title" class="row-fluid">
	<div class="page-header">
		<h1>Select and book a prayer slot</h1>
	</div>
</section><!--title-->

<section id="controls" class="row-fluid text_center">
	<div class="span3 cell03">
		<?php 
			echo CHtml::link(
				'<i class="icon-arrow-left"></i>',
				array(
					'bookings/planner',
					'wid'=>$this->getPreviousWeekLink(),
				),
				array(
					'class'=>'nounderline cellbutton',
				)
			); 
		?>
	</div> <!-- span3 -->
	
	<div class="span18 cell01">
		<span class="quiet">
			<?php echo 'Week: '.$this->getWeekReadableDateRange(); ?>
		</span>
	</div> <!-- span18 -->
	
	<div class="span3 cell03">
		<?php 
			echo CHtml::link(
				'<i class="icon-arrow-right"></i>',
				array(
					'bookings/planner',
					'wid'=>$this->getNextWeekLink(),
				),
				array(
					'class'=>'nounderline cellbutton',
				)
			); 
		?>
	</div> <!-- span3 -->
</section><!--controls-->

<section id="calandar" class="text_center">
<?php 
	// Loop through the hours of the day
	for ($i=0;$i<count($plannerRowTitle);$i++) {
		$row_id = ($plannerRowTitle[$i] != 'Time') ? 'row'.substr($plannerRowTitle[$i],0,2) : '';
		
		echo '<section id="'.$row_id.'" class="row-fluid">';
			echo '<div class="row-fluid">';
				echo '<div class="span3 cell01">
								<span class="quiet">'.$plannerRowTitle[$i].'</span>
						</div> <!-- span3 -->';
						
				if (($i == 0) || ($i == (count($plannerRowTitle) - 1))) {
					for ($j=1;$j<=count($plannerColumnTitle);$j++) {
						//$last_class = ($j == count($plannerColumnTitle)) ? ' last' : '';
						echo '<div class="span3 cell01">
								<span>' . $plannerColumnTitle[$j] . '</span><br />
								<span class="quiet small">' . $this->getDayReadableDateInfo($j - 1) . '</span>
							</div> <!-- span3 -->';
					}
				} else {
					for ($j=1;$j<=count($plannerColumnTitle);$j++) {
						
						$hour_id = $this->getHourNumberFromHourAndDayID($i,$j);
						$status_class = $this->getBookingCSSStatusForHour($hour_id);
						
						$output = '';
						$output .= '<div class="span3 cell02 '.$status_class.'">';
						
						switch ($status_class) {
							case 'button01': // Normal Button
								$output2 = '';
								$output2 .= '<div class="row-fluid">';
								$output2 .= '	<div class="span24">';
								$output2 .= '		<h6>Available</h6>';
								$output2 .= '	</div><!-- span24 -->';
								$output2 .= '</div><!-- row-fluid -->';
								$output2 .= '<div class="row-fluid">';
								$output2 .= '	<div class="span24">';
								$output2 .= 			$this->getBookingHTMLContentForHour($hour_id,2);
								$output2 .= '	</div><!-- span24 -->';
								$output2 .= '</div><!-- row-fluid -->';
								
								/*if ( $isAdminUser ) {
									$output2 .= '<div class="row-fluid">';
									$output2 .= '	<div class="span24" style="border-top:1px solid rgba(0,0,0,0.05);padding:8px 0px;">';
									$output2 .=  		CHtml::link(
															'<i class="icon-edit"></i> Manage',
															array(
																'bookings/create',
																'wid'=>$specifiedWeek,
																'hid'=>$hour_id,
															),
															array(
																'class'=>'btn btn-small',
																'style'=>'padding:4px 0px;width:85%;',
																'title'=>'Book this prayer slot',
															)
														);
									$output2 .= '	</div><!-- span24 -->';
									$output2 .= '</div><!-- row-fluid -->';
								}*/
								
								//if ((time() < $this->getTimetampFromWIDandDID($specifiedWeek, $hour_id) ) || (Yii::app()->user->id > 0)) {	
									$output .= CHtml::link(
										$output2,
										'#modal',
										//array(
										//	'bookings/create',
										//	'wid'=>$specifiedWeek,
										//	'hid'=>$hour_id,
										//),
										array(
											'class'=>'cellbutton nounderline bookslot',
											'title'=>'Book the prayer slot.',
											'onclick'=>"{setWeekID({$specifiedWeek});setHourID({$hour_id});addBooking();}",
											'data-toggle'=>'modal',
										)
									);
								//} else {
									//$output .= $output2;
								//}
								break;
							case 'button02': // Locked Button
								$output2 = '';
								$output2 .= '<div class="row-fluid">';
								$output2 .= '	<div class="span24">';
								$output2 .= '		<h6>Private</h6>';
								$output2 .= '	</div><!-- span24 -->';
								$output2 .= '</div><!-- row-fluid -->';
								//if (Yii::app()->user->id > 0) {
								//	$output2 .= '<div class="row-fluid">';
								//	$output2 .= '	<div class="span24" style="padding-bottom:8px;">';
								//	$output2 .=  		CHtml::link(
								//							'<i class="icon-edit"></i> Manage',
								//							array(
								//								'bookings/create',
								//								'wid'=>$specifiedWeek,
								//								'hid'=>$hour_id,
								//							),
								//							array(
								//								'class'=>'btn btn-small',
								//								'style'=>'padding:4px 0px;width:85%;',
								//								'title'=>'Book this prayer slot',
								//							)
								//						);
								//	$output2 .= '	</div><!-- span24 -->';
								//	$output2 .= '</div><!-- row-fluid -->';
								//}
								$output2 .= '<div class="row-fluid">';
								$output2 .= '	<div class="span24">';
								$output2 .= 			$this->getBookingHTMLContentForHour($hour_id,2);
								$output2 .= '	</div><!-- span24 -->';
								$output2 .= '</div><!-- row-fluid -->';
								
								/* if ( $isAdminUser ) {
									$output2 .= '<div class="row-fluid">';
									$output2 .= '	<div class="span24" style="border-top:1px solid rgba(0,0,0,0.05);padding:8px 0px;">';
									$output2 .=  		CHtml::link(
															'<i class="icon-edit"></i> Manage',
															array(
																'bookings/create',
																'wid'=>$specifiedWeek,
																'hid'=>$hour_id,
															),
															array(
																'class'=>'btn btn-small',
																'style'=>'padding:4px 0px;width:85%;',
																'title'=>'Book this prayer slot',
															)
														);
									$output2 .= '	</div><!-- span24 -->';
									$output2 .= '</div><!-- row-fluid -->';
								} */
								
								if (Yii::app()->user->id > 0) {	
									$output .=  CHtml::link(
										$output2,
										array(
											'bookings/create',
											'wid'=>$specifiedWeek,
											'hid'=>$hour_id,
										),
										array(
											'class'=>'cellbutton nounderline bookslot',
											'title'=>'Manage Prayer Slot',
										)
									);
								} else {
									$output .= $output2;
								}
								break;
							case 'button03': // Disabled Button
								$output2 = '';
								$output2 .= '<div class="row-fluid">';
								$output2 .= '	<div class="span24">';
								$output2 .= '		<h6>Unavailable</h6>';
								$output2 .= '	</div><!-- span24 -->';
								$output2 .= '</div><!-- row-fluid -->';
								//if (Yii::app()->user->id > 0) {
								//	$output .= '<div class="row-fluid">';
								//	$output .= '	<div class="span24" style="padding-bottom:8px;">';
								//	$output .=  		CHtml::link(
								//							'<i class="icon-edit"></i> Manage',
								//							array(
								//								'bookings/create',
								//								'wid'=>$specifiedWeek,
								//								'hid'=>$hour_id,
								//							),
								//							array(
								//								'class'=>'btn btn-small',
								//								'style'=>'padding:4px 0px;width:85%;',
								//								'title'=>'Book this prayer slot',
								//							)
								//						);
								//	$output .= '	</div><!-- span24 -->';
								//	$output .= '</div><!-- row-fluid -->';
								//}
								$output2 .= '<div class="row-fluid">';
								$output2 .= '	<div class="span24">';
								$output2 .= 			$this->getBookingHTMLContentForHour($hour_id,4);
								$output2 .= '	</div><!-- span24 -->';
								$output2 .= '</div><!-- row-fluid -->';
								
								/*if ( $isAdminUser ) {
									$output2 .= '<div class="row-fluid">';
									$output2 .= '	<div class="span24" style="border-top:1px solid rgba(0,0,0,0.05);padding:8px 0px;">';
									$output2 .=  		CHtml::link(
															'<i class="icon-edit"></i> Manage',
															array(
																'bookings/create',
																'wid'=>$specifiedWeek,
																'hid'=>$hour_id,
															),
															array(
																'class'=>'btn btn-small',
																'style'=>'padding:4px 0px;width:85%;',
																'title'=>'Book this prayer slot',
															)
														);
									$output2 .= '	</div><!-- span24 -->';
									$output2 .= '</div><!-- row-fluid -->';
								}*/
									
								$output .= $output2;
								break;
						}
						$output .= '</div> <!-- span3 -->';
						echo $output;
					}
				}
			echo '</div><!--row-fluid-->';
			echo '<div class="row-fluid">';
				if (($i > 0) && ($i < (count($plannerRowTitle) - 1)) && ($isAdminUser)) {
					echo '<div class="span3">&nbsp;</div>';
					
					for ($j=1;$j<=count($plannerColumnTitle);$j++) {
						$hour_id = $this->getHourNumberFromHourAndDayID($i,$j);
						
						$output2 = '<div class="span3" style="padding-bottom:10px;">';
						$output2 .=  CHtml::link(
								'<i class="icon-edit"></i> Manage',
								array(
									'bookings/create',
									'wid'=>$specifiedWeek,
									'hid'=>$hour_id,
								),
								array(
									'class'=>'btn btn-small',
									'style'=>'padding:4px 0px;width:8em;',
									'title'=>'Book this prayer slot',
								)
							);
						$output2 .= '</div>';
						echo $output2;
					}
					
				}
			echo '</div><!--row-fluid-->';
		echo '</section><!--row-fluid-->';
	}
?>

</section><!--calandar-->

<?php $this->beginWidget('bootstrap.widgets.BootModal', array(
    'id'=>'modal',
    'htmlOptions'=>array('class'=>'hide'),
)); ?>
<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h1>Book</h1>
</div>
<div class="modal-body"></div>
<div class="modal-footer"></div>
<?php $this->endWidget(); ?> 

<script type="text/javascript">

var week_id;
var hour_id;

function setWeekID( specified_wid ) {
	week_id = specified_wid;
	//alert(window.week_id);
}

function setHourID( specified_hid ) {
	hour_id = specified_hid;
	//alert(window.hour_id);
}

function addBooking() {
	
	baseurl = <?php echo CJSON::encode(Yii::app()->createUrl('bookings/create')); ?>;
	ajax_url = baseurl + '&wid=' + window.week_id + '&hid=' + window.hour_id;
	returnURL = <?php echo CJSON::encode(Yii::app()->createUrl('bookings/planner')); ?> + '&wid=' + window.week_id;
	
	<?php 
    	echo CHtml::ajax(
    		array(
            	'url'=>"js:ajax_url",
            	'data'=> "js:$(this).serialize()",
            	'type'=>'post',
            	'dataType'=>'json',
            	'success'=>"function(data)
            	{
                	if (data.status == 'failure')
                	{
                    	$('#modal div.modal-body').html(data.div);
                        // Here is the trick: on submit-> once again this function!
                    	$('#modal div.modal-body form').submit(addBooking);
                	}
                	else
                	{
                    	$('#modal div.modal-body').html(data.div);
                    	setTimeout(\"window.location.href = returnURL \",2000);
                	}
 
            	} ",
        	)
        )
    ?>
    return false; 
    
}
</script>