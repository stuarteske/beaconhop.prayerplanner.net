<div class="form">

<?php 
	$form = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
    	'id'=>'bookings-form',
    	'type'=>'inline',
    	'enableClientValidation'=>true,
    	'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
    	'htmlOptions'=>array('class'=>'well'),
	));
	
	$userModel=new User;
	$userRecord=$userModel->findByPk(Yii::app()->user->id);
	$nameTextField = (isset($userRecord->name)) ? $userRecord->name : '';
	$emailTextField = (isset($userRecord->username)) ? $userRecord->username : '';
?>

<?php 
	if ($model->isNewRecord)
		echo '<p class="note">Book the slot '.$this->getReadableDateFromWIDandDID($wid,$hid).'</p>';
?>

<?php echo $form->errorSummary($model); ?>

<?php 
	if ($model->isNewRecord)
		echo $form->hiddenField($model,'slot_booked',array('value'=>$this->getSQLDateFromWIDandDID($wid,$hid)));
?>

<!-- <div class="row"> -->
	<?php //echo $form->labelEx($model,'booked_user_id'); ?>
	<?php //echo $form->textField($model,'booked_user_id',array('size'=>10,'maxlength'=>10)); ?>
	<?php //echo $form->error($model,'booked_user_id'); ?>
<!-- </div> -->

<?php 
		if ($model->isNewRecord) {
			$output = '';
			$output .= '<div class="control-group">';
			$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'users_name') .'</label>';
			$output .= '<div class="controls">';
			$output .= $form->textFieldRow($model, 'users_name', array('value'=>CHtml::encode($nameTextField),'class'=>'span10','maxlength'=>64));
			$output .= '<span class="field-error"><strong>'.$form->error($model,'users_name').'</strong></span>';
			$output .= '</div>';
			$output .= '</div>';
			echo $output;
		} else {
			$output = '';
			$output .= '<div class="control-group">';
			$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'users_name') .'</label>';
			$output .= '<div class="controls">';
			$output .= $form->textFieldRow($model, 'users_name', array('class'=>'span10','maxlength'=>64));
			$output .= '<span class="field-error"><strong>'.$form->error($model,'users_name').'</strong></span>';
			$output .= '</div>';
			$output .= '</div>';
			echo $output;
		}
	?>

<?php 
		if ($model->isNewRecord) {
			$output = '';
			$output .= '<div class="control-group">';
			$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'username') .'</label>';
			$output .= '<div class="controls">';
			$output .= $form->textFieldRow($model, 'username', array('value'=>CHtml::encode($emailTextField),'class'=>'span10','maxlength'=>128));
			$output .= '<span class="field-error"><strong>'.$form->error($model,'username').'</strong></span>';
			$output .= '</div>';
			$output .= '</div>';
			echo $output;
		} else {
			$output = '';
			$output .= '<div class="control-group">';
			$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'username') .'</label>';
			$output .= '<div class="controls">';
			$output .= $form->textFieldRow($model, 'username', array('class'=>'span10','maxlength'=>128));
			$output .= '<span class="field-error"><strong>'.$form->error($model,'username').'</strong></span>';
			$output .= '</div>';
			$output .= '</div>';
			echo $output;
		}
	?>

<!-- <div class="row"> -->
	<?php // echo $form->labelEx($model,'edited_user_id'); ?>
	<?php // echo $form->textField($model,'edited_user_id',array('size'=>10,'maxlength'=>10)); ?>
	<?php // echo $form->error($model,'edited_user_id'); ?>
<!-- </div> -->

<!-- <div class="row"> -->
	<?php // echo $form->labelEx($model,'time_created'); ?>
	<?php // echo $form->textField($model,'time_created'); ?>
	<?php // echo $form->error($model,'time_created'); ?>
<!-- </div> -->

<!-- <div class="row"> -->
	<?php // echo $form->labelEx($model,'time_edited'); ?>
	<?php // echo $form->textField($model,'time_edited'); ?>
	<?php // echo $form->error($model,'time_edited'); ?>
<!-- </div> -->

	<?php //echo $form->labelEx($model,'slot_locked'); ?>
	<?php //echo $form->dropDownList($model,'slot_locked',$model->getLockedOptions()); ?>
	<?php //echo $form->error($model,'slot_locked'); ?>
<?php
	$output = '';	
	$output .= '<div class="control-group">';
	$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'slot_locked') .'</label>';
	$output .= '<div class="controls">';
	$output .= $form->dropDownList($model,'slot_locked',$model->getLockedOptions());
	$output .= '<span class="field-error"><strong>'.$form->error($model,'slot_locked').'</strong></span>';
	$output .= '</div>';
	$output .= '</div>';
	echo $output;
?>
	
<?php
	$output = '';
	if (Yii::app()->user->id > 0) {	
		$output .= '<div class="control-group">';
		$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'slot_disabled') .'</label>';
		$output .= '<div class="controls">';
		$output .= $form->dropDownList($model,'slot_disabled',$model->getLockedOptions());
		$output .= '<span class="field-error"><strong>'.$form->error($model,'slot_disabled').'</strong></span>';
		$output .= '</div>';
		$output .= '</div>';
	} else {
		$output .= $form->hiddenField($model,'slot_disabled',array('value'=>0));
	}
	echo $output;
?>

<?php
	if ($model->isNewRecord) {
		$output = '';
		$output .= '<div class="control-group">';
		$output .= '<label class="control-label" for="inputIcon">Book Consecutive Hours</label>';
		$output .= '<div class="controls">';
		$output .= CHtml::dropDownList('block_booking', Yii::app()->request->getParam('block_booking', 1), $model->getBlockBookingValues());
		$output .= '<span class="field-error"><strong></strong></span>';
		$output .= '</div>';
		$output .= '</div>';
		echo $output;
	}
?>
	
<?php echo CHtml::htmlButton($model->isNewRecord ? '<i class="icon-ok"></i> Book Prayer Slot' : '<i class="icon-ok"></i> Save Changes', array('class'=>'btn', 'type'=>'submit')); ?>

<?php $this->endWidget(); ?>

</div><!-- form -->