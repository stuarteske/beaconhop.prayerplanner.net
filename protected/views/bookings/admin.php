<?php
$this->breadcrumbs=array(
	'Bookings'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Manage Bookings','url'=>array('admin'), 'active'=>true),
	array('label'=>'List Bookings','url'=>array('index')),
	//array('label'=>'Create Bookings','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-grid', {
		data: $(this).serialize()
	});
	return false;
});");
?>

<div class="page-header">
	<h1>Manage Bookings <small>Use this tool to manage the system's bookings</small></h1>
</div>

<div class="alert alert-info">
	<a class="close" data-dismiss="alert">×</a>
	<strong>Search Instructions</strong> You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.'
</div>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>

<div class="search-form well" style="display:none">

<?php $this->renderPartial('_search',array(

	'model'=>$model,

)); ?>
</div><!-- search-form -->

<?php
/*
 * @property string $id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $last_login_time
 * @property string $create_time
 * @property string $create_user_id
 * @property string $update_time
 * @property string $update_user_id
 */
?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(

	'id'=>'users-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		'id',
		/*'booked_user_id',*/
		'users_name',
		'username',
		/*'edited_user_id',*/
		'time_created',
		/*'time_edited',*/
		'slot_booked',
		'slot_locked',
		'slot_disabled',
		array(
			'class'=>'CButtonColumn',
		),
	),

)); ?>

