<?php
$this->breadcrumbs=array(
	'Bookings',
);

$this->menu=array(
	array('label'=>'List Bookings','url'=>array('index'), 'active'=>true),
	//array('label'=>'Create Bookings','url'=>array('create')),
	array('label'=>'Manage Bookings','url'=>array('admin')),
);
?>

<div class="page-header">
	<h1>Bookings <small>View the complete bookings list</small></h1>
</div>

<div class="">
<?php 

/* 
 * @property string $id
 * @property string $booked_user_id
 * @property string $users_name
 * @property string $username
 * @property string $edited_user_id
 * @property string $time_created
 * @property string $time_edited
 * @property string $slot_booked
 * @property integer $slot_locked
 * @property integer $slot_disabled
 */

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'users-grid',
	'dataProvider'=>$dataProvider,
	//'filter'=>$model,
	'columns'=>array(
		'id',
		'users_name',
		'username',
		'time_created',
		'slot_booked',
		'slot_locked',
		'slot_disabled',
		array(
			'class'=>'CLinkColumn',
            'urlExpression'=>'Yii::app()->createUrl("bookings/view", array("id"=>$data->id))',
            'label'=>'View Booking',
        ),
	),
));

?>


</div>
