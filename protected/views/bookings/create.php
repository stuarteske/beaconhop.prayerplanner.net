<?php
$this->breadcrumbs=array(
	'Book',
);

//$this->menu=array(
//	array('label'=>'List Bookings', 'url'=>array('index')),
//	array('label'=>'Manage Bookings', 'url'=>array('admin')),
//);
?>

<div class="row-fluid">

<div class="page-header">
	<h1>Book Slot <small><?php echo $this->getReadableDateFromWIDandDID($wid,$hid); ?></small></h1>
</div>

<?php 
echo $this->renderPartial(
	'_form',
	array(
		'model'=>$model,
		'wid'=>$wid,
		'hid'=>$hid,
	)
);
?>

<?php 
$userModel = new User;

if (($userModel->isAdminUser(Yii::app()->user->id)) && ($model->isNewRecord)) {
	
	echo '<div class="page-header">';
	echo '	<h1>Manage Bookings <small>Manage the bookings for this prayer slot</small></h1>';
	echo '</div>';

	$this->widget('bootstrap.widgets.BootGridView', array(
    	'id'=>'bookings-grid',
    	'dataProvider'=>$dataProvider,
    	//'template'=>"{items}",
    	'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    	'columns'=>array(
			'id',
			//'booked_user_id',
			'users_name',
			'username',
			//'edited_user_id',
			'time_created',
			//'time_edited',
			'slot_booked',
			'slot_locked',
			'slot_disabled',
			array(
            	'class'=>'bootstrap.widgets.BootButtonColumn',
            	'htmlOptions'=>array('style'=>'width: 50px'),
        	),
		),
	));
}
?>

</div><!-- row-fluid -->