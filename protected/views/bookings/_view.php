<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('users_name')); ?>:</b>
	<?php echo CHtml::encode($data->users_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('slot_booked')); ?>:</b>
	<?php echo CHtml::encode($data->slot_booked); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('slot_locked')); ?>:</b>
	<?php echo CHtml::encode($data->getLockedText()); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('slot_disabled')); ?>:</b>
	<?php echo CHtml::encode($data->getDisabledText()); ?>
	<br />
	
	<?php echo CHtml::Button('Edit', array('submit'=>'#')); ?>
	<?php 
		echo CHtml::Button(
			'Delete',
			array(
				'submit'=>Yii::app()->createUrl('bookings/delete', array('id'=>$data->id)) 
			)
		); 
	?>
	<br />

</div>