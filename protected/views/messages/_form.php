<div class="form">

<?php $form = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
    	'id'=>'messages-form',
    	'type'=>'inline',
    	'enableClientValidation'=>true,
    	'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
    	'htmlOptions'=>array('class'=>'well'),
	));
?>

	<p class="note">Report a bug or issue using this form. Tell us what you were doing, the URL you were using and what happened. You have 250 characters for this message.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php
	$output = '';
	$output .= '<div class="control-group">';
	$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'msg') .'</label>';
	$output .= '<div class="controls">';
	$output .= $form->textFieldRow($model, 'msg', array('class'=>'span10','maxlength'=>250));
	$output .= '<span class="field-error"><strong>'.$form->error($model,'msg').'</strong></span>';
	$output .= '</div>';
	$output .= '</div>';
	echo $output;
	?>

	<?php echo CHtml::htmlButton($model->isNewRecord ? '<i class="icon-ok"></i> Submit' : '<i class="icon-ok"></i> Save Changes', array('class'=>'btn', 'type'=>'submit')); ?>

<?php $this->endWidget(); ?>

</div><!-- form -->