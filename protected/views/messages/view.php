<?php
$this->breadcrumbs=array(
	'Bugs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'View Bug #'.$model->id,'url'=>array('view','id'=>$model->id),'active'=>true),
	array('label'=>'List Bugs','url'=>array('index')),
	//array('label'=>'Create Bookings','url'=>array('create')),
	array('label'=>'Update Bugs','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Bug','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Bugs','url'=>array('admin')),
);
?>

<div class="page-header">
	<h1>View Bug #<?php echo $model->id; ?> <small>Details for Bug #<?php echo $model->id; ?></small></h1>
</div>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'msg',
	),
)); ?>