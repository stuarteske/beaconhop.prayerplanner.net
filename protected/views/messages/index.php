<?php
$this->breadcrumbs=array(
	'Bugs',
);

$this->menu=array(
	array('label'=>'List Bugs','url'=>array('index'), 'active'=>true),
	//array('label'=>'Create Bug','url'=>array('create')),
	array('label'=>'Manage Bugs','url'=>array('admin')),
);
?>

<div class="page-header">
	<h1>Bugs <small>View the complete Bug list</small></h1>
</div>

<div class="">
<?php 

/*
* @property integer $id
 * @property string $msg
 */

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'users-grid',
	'dataProvider'=>$dataProvider,
	//'filter'=>$model,
	'columns'=>array(
		'id',
		'msg',
		array(
			'class'=>'CLinkColumn',
            'urlExpression'=>'Yii::app()->createUrl("messages/view", array("id"=>$data->id))',
            'label'=>'View Bug',
        ),
	),
));

?>


</div>
