<?php
$this->breadcrumbs=array(
	'Report a Bug',
);?>

<div class="row-fluid">

<div class="page-header">
	<h1>Report a Bug <small><?php echo 'Use this form to report a bugs and issues'; ?></small></h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

</div><!-- row-fluid -->