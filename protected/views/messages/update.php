<?php
$this->breadcrumbs=array(
	'Bugs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Update Bug #'.$model->id,'url'=>array('update','id'=>$model->id),'active'=>true),
	array('label'=>'List Bugs','url'=>array('index')),
	//array('label'=>'Create Bookings','url'=>array('create')),
	array('label'=>'View Bugs','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Bugs','url'=>array('admin')),
);
?>

<div class="page-header">
	<h1>Update Bug #<?php echo $model->id; ?> <small>Make changes to this Bug</small></h1>
</div>

<div class="">
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
</div>