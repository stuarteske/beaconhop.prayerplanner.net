<?php $this->beginContent('//layouts/main'); ?>

<div id="content" class="row-fluid">
	<div id="" class="span24">
		<?php 
			$this->widget('bootstrap.widgets.BootMenu', array(
				'type'=>'pills', // '', 'tabs', 'pills' (or 'list')
				'stacked'=>false, // whether this is a stacked menu
				'items'=>$this->menu,
			)); 
		?>
	</div><!-- sidebar -->
</div> <!-- row-fluid -->

<div id="content" class="row-fluid">
	<div id="content" class="span24">
		<?php echo $content; ?>
	</div><!-- content -->
</div> <!-- row-fluid -->

<?php $this->endContent(); ?>