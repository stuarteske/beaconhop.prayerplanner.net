<?php
	//if (eregi("MSIE",getenv("HTTP_USER_AGENT")) ||
    //	eregi("Internet Explorer",getenv("HTTP_USER_AGENT"))) {	
	//		Yii::app()->user->setFlash('error', "<strong>Warning</strong> Internet Explora has been detected, upgrade your browser immediately!");	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<meta name="keywords" content="christian, organisation, prayer, planner, pray planner, managment, software" />
	<meta name="description" content="High performance prayer planner managment software." />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<!-- Javascript -->
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.1.7.2.min.js"></script>
	
	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/planner.css" />
	<!-- <link rel="stylesheet" type="text/css" href="<?php //echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />-->
	<link rel="icon" type="image/png" href="<?php echo Yii::app()->request->baseUrl.'/images/favicon.png'; ?>" />
</head>
<body style="padding-top:50px;min-width:960px;">
<header id="header" class="container-fluid">
<?php 
	$userModel = new User;
	$this->widget('bootstrap.widgets.BootNavbar', array(
    'fixed'=>true,
    'fluid'=>true,
    'brand'=>CHtml::encode(Yii::app()->name),
    //'brand'=>'',
    'brandUrl'=>Yii::app()->request->baseUrl,
    //'brandUrl'=>'',
    'collapse'=>false, // requires bootstrap-responsive.css
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.BootMenu',
            'items'=>array(
                array('label'=>'Planner', 'url'=>array('/bookings/planner')),
                array('label'=>'Instructions', 'url'=>array('/site/page', 'view'=>'instructions')),
                //array('label'=>'v1.01a', 'url'=>array('/site/page', 'view'=>'version')),
            ),
        ),
        array(
            'class'=>'bootstrap.widgets.BootMenu',
            'htmlOptions'=>array('class'=>'pull-right'),
            'items'=>array(
                array(
                	'label'=>'Login', 
                	'url'=>array('/site/login'),
                	'visible'=>Yii::app()->user->isGuest,
                ),
                array( 
                	'label'=>'Account',
                	'visible'=>!Yii::app()->user->isGuest,
                	'items'=>array(
                		array(
                    		'label'=>'Admin',
                    		'visible'=>$userModel->isAdminUser(Yii::app()->user->id),
                    	),
                		array(
                    		'label'=>'- Manage Bookings',
                    		'url'=>array('/bookings/admin'),
                    		'visible'=>$userModel->isAdminUser(Yii::app()->user->id),
                    	),
                    	array(
                    		'label'=>'- Manage Bugs',
                    		'url'=>array('/messages/admin'),
                    		'visible'=>$userModel->isAdminUser(Yii::app()->user->id),
                    	),
                    	array(
                    		'label'=>'- Manage Users',
                    		'url'=>array('/user/admin'),
                    		'visible'=>$userModel->isAdminUser(Yii::app()->user->id),
                    	),
                    	array(
                    		'label'=>Yii::app()->user->name,
                    	),
                    	array(
                    		'label'=>'- Profile',
                    		'url'=>array('/user/update', 'id'=>Yii::app()->user->id),
                    	),
                    	array(
                    		'label'=>'- Logout',
                    		'url'=>array('/site/logout'),
                    	),
                	)
                ),
                array('label'=>'Contact', 'url'=>array('/site/contact')),
            ),
        ),
    ),
)); ?>
</header><!-- header -->

<div class="container-fluid">
	<div id="title-hero" class="hero-unit">
		<div class="row">
			<div class="span4">
				<!-- <?php echo Yii::app()->request->baseUrl; ?> -->
				<img id="logo-image" src="<?php echo Yii::app()->request->baseUrl . '/' . 'images/logo.png'; ?>" alt="Prayer Planner" />
			</div><!-- span4 -->
			<div class="span18">
				<div id="title-text">
					<h1><?php echo CHtml::encode(Yii::app()->name); ?></h1>
					<?php echo '<h5>'.date('l, jS \of F Y - G:ia (T P e)').'</h5>'; ?>
				</div>
			</div><!-- span20 -->
		</div><!-- row -->
	</div><!-- hero-unit -->
	
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.BootBreadcrumbs', array(
    		'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>
	
	<?php $this->widget('bootstrap.widgets.BootAlert'); ?>

	<?php echo $content; ?>

	<div class="clear"></div>
	
</div><!-- container-fluid -->

<footer class="text_center">
	<hr />
	<p class="copy">
		Copyright &copy; <?php echo date('Y'); ?> <a href="http://www.prayerplanner.net/?_=beaconhop"target="_parent">PrayerPlanner.net.</a>
		All Rights Reserved.
	</p>

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-32810323-3']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</footer><!-- footer -->
</body>
</html>
