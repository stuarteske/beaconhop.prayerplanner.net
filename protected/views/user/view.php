<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'View User #'.$model->id,'url'=>array('view','id'=>$model->id),'active'=>true),
	array('label'=>'List Users','url'=>array('index')),
	array('label'=>'Create Users','url'=>array('create')),
	array('label'=>'Update Users','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete User','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Users','url'=>array('admin')),
);
?>

<div class="page-header">
	<h1>View User #<?php echo $model->id; ?> <small>Details for user <?php echo $model->id; ?></small></h1>
</div>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'name',
		'last_login_time',
		'create_time',
		'update_time',
		'system_admin',
	),
)); ?>

