<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
    'id'=>'users-form',
    'type'=>'inline',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
    'htmlOptions'=>array('class'=>'well'),
));

$userModel = new User;
?>
	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<?php 
		$output = '';
		$output .= '<div class="control-group">';
		$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'username') .'</label>';
		$output .= '<div class="controls">';
		$output .= $form->textFieldRow($model, 'username', array('class'=>'span5','maxlength'=>128));
		$output .= '<span class="field-error"><strong>'.$form->error($model,'username').'</strong></span>';
		$output .= '</div>';
		$output .= '</div>';
		echo $output;
	?>

	
	<?php 
		$output = '';
		$output .= '<div class="control-group">';
		$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'name') .'</label>';
		$output .= '<div class="controls">';
		$output .= $form->textFieldRow($model, 'name', array('class'=>'span5','maxlength'=>64));
		$output .= '<span class="field-error"><strong>'.$form->error($model,'name').'</strong></span>';
		$output .= '</div>';
		$output .= '</div>';
		echo $output;
	?>
	
	<?php 
		$output = '';
		$output .= '<div class="control-group">';
		$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'password') .'</label>';
		$output .= '<div class="controls">';
		$output .= $form->textFieldRow($model, 'password', array('class'=>'span5','maxlength'=>64, 'value'=>''));
		$output .= '<span class="field-error"><strong>'.$form->error($model,'password').'</strong></span>';
		$output .= '</div>';
		$output .= '</div>';
		echo $output;
	?>

	
	<?php 
		$output = '';
		$output .= '<div class="control-group">';
		$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'password_repeat') .'</label>';
		$output .= '<div class="controls">';
		$output .= $form->textFieldRow($model, 'password_repeat', array('class'=>'span5','maxlength'=>64, 'value'=>''));
		$output .= '<span class="field-error"><strong>'.$form->error($model,'password_repeat').'</strong></span>';
		$output .= '</div>';
		$output .= '</div>';
		echo $output;
	?>
	
	<?php
		if ($userModel->isAdminUser(Yii::app()->user->id)) {
			$output = '';	
			$output .= '<div class="control-group">';
			$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'system_admin') .'</label>';
			$output .= '<div class="controls">';
			$output .= $form->dropDownList($model,'system_admin',$model->getAdminOptions());
			$output .= '<span class="field-error"><strong>'.$form->error($model,'system_admin').'</strong></span>';
			$output .= '</div>';
			$output .= '</div>';
			echo $output;
		}
	?>
	
	<?php echo CHtml::htmlButton('<i class="icon-ok"></i>'.$model->isNewRecord ? ' Create' : ' Save', array('class'=>'btn', 'type'=>'submit')); ?>

<?php $this->endWidget(); ?>
