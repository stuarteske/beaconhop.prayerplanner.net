<?php
$userModel = new User;

if ($userModel->isAdminUser(Yii::app()->user->id)) {
	$this->breadcrumbs=array(
		'Users'=>array('index'),
		$model->id=>array('view','id'=>$model->id),
		'Update',
	);

	$this->menu=array(
		array('label'=>'Update User #'.$model->id,'url'=>array('update','id'=>$model->id),'active'=>true),
		array('label'=>'List Users','url'=>array('index')),
		array('label'=>'Create Users','url'=>array('create')),
		array('label'=>'View Users','url'=>array('view','id'=>$model->id)),
		array('label'=>'Manage Users','url'=>array('admin')),
	);
} else if (Yii::app()->user->id != $model->id) throw new CHttpException(400,'Invalid request. Please do not repeat this request again. This action has been reported!')
?>

<div class="page-header">
	<h1>Update User #<?php echo $model->id; ?> <small>Make changes here</small></h1>
</div>

<div class="">
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
</div>