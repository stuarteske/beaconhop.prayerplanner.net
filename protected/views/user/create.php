<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'Create Users','url'=>array('create'), 'active'=>true),
	array('label'=>'List Users','url'=>array('index')),
	array('label'=>'Manage Users','url'=>array('admin')),
);
?>

<div class="page-header">
	<h1>Create Users <small>Add a user to your database</small></h1>
</div>

<div class="">
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>