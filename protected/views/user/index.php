<?php
$this->breadcrumbs=array(
	'Users',
);

$this->menu=array(
	array('label'=>'List Users','url'=>array('index'), 'active'=>true),
	array('label'=>'Create Users','url'=>array('create')),
	array('label'=>'Manage Users','url'=>array('admin')),
);
?>

<div class="page-header">
	<h1>Users <small>View the complete user list</small></h1>
</div>

<div class="">
<?php 

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'users-grid',
	'dataProvider'=>$dataProvider,
	//'filter'=>$model,
	'columns'=>array(
		'id',
		'username',
		'name',
		'create_time',
		'last_login_time',
		'update_time',
		'system_admin',
		array(
			'class'=>'CLinkColumn',
            'urlExpression'=>'Yii::app()->createUrl("user/view", array("id"=>$data->id))',
            'label'=>'View User',
        ),
	),
));

?>


</div>


