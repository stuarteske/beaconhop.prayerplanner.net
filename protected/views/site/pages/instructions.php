<?php
$this->pageTitle=Yii::app()->name . ' - Instructions';
$this->breadcrumbs=array(
	'Instructions',
);
?>
<?php $this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked'=>false, // whether this is a stacked menu
    'items'=>array(
        array('label'=>'General', 'url'=>'#general', 'active'=>true),
        array('label'=>'Admin', 'url'=>'#admin'),
    ),
)); ?>
<section id="v100" class="row-fluid">
	<div id="general" class="page-header">
		<h1>General Instructions <small>Discover how to operate this application</small></h1>
	</div>
	<section id="g01" class="well">
		<h3>How to book a slot?</h3>
		<p>To book a prayer slot touch the desired time plannel within the planner. You will be directed to the booking for where you should enter your name and email address. Touch book the slot when ready</p>
		<p>If you have a user account? Be sure to login before booking your slot. The system will enter your name and email address for you.</p>
	</section> <!-- row-fluid -->
	<hr />
	<section id="g02" class="well">
		<h3>How do I navigate to a different week?</h3>
		<p>On the planner page you will see two panels containing arrows. Touch the left or right facing arrows to move backwards or forwards in time.</p>
	</section> <!-- row-fluid -->
	<hr />
	<section id="g03" class="well">
		<h3>Why can I not book the hour I want?</h3>
		<p>There can be several reasons for this. Has the desired slot already expired? This will happen if you are trying to book a slot which has passed by already.</p>
		<p>If the prayer slot title reads "Unavailable" and has a red tint the slot has been disabled.</p>
		<p>If the prayer slot title reads "Private" and has a blue tint the slot has been set to private. You can not join in with this prayer session.</p>
	</section> <!-- row-fluid -->
	<div id="admin" class="page-header">
		<h1>Management Instructions <small>You must be logged in as an admin </small></h1>
	</div>
	<section id="m01" class="well">
		<h3>How do I disable a prayer slot?</h3>
		<p>When logged in touch the desired prayer slot. Book the prayer slot as an admin and select yes for the disable the prayer slot drop down.</p>
	</section> <!-- row-fluid -->
	<hr />
	<section id="m02" class="well">
		<h3>How do I make a prayer slot available?</h3>
		<p>When logged in touch the desired prayer slot. Look through the list of people which have booked the slot. Find the person who has disabled the slot. The persons booking can either be deleted or updated.</p>
	</section> <!-- row-fluid -->
</section><!-- row-fluid -->