<?php
$this->pageTitle=Yii::app()->name . ' - Upgrade your browser!';
$this->breadcrumbs=array(
	'Upgrade',
);
?>
<section id="v100" class="row-fluid">
	<div id="general" class="page-header">
		<h1>Internet Explorer <small>You browser is a major security concern!</small></h1>
	</div>
	<div class="alert alert-error">
		<a class="close" data-dismiss="alert">×</a>
		<strong>Warning: Internet Explorer</strong> has been detected, you must upgrade your browser Immediately!
	</div>
	<section id="g01" class="well">
		<h3>How do I upgrade?</h3>
		<p>To upgrade your browser click one of the links below. Three of the most popular browsers have been selected for you. The browsers have been listed in order of popularity.</p>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Browser</th>
				<th>Link</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>Firefox</td>
				<td>
					<?php $this->widget('bootstrap.widgets.BootButton', array(
        				'label'=>'Download',
        				'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        				'size'=>'Large', // '', 'small' or 'large'
        				'url'=>'http://www.mozilla.org/en-US/firefox/new/',
    				)); ?>
				</td>
			</tr>
			<tr>
				<td>2</td>
				<td>Google Chrome</td>
				<td>
					<?php $this->widget('bootstrap.widgets.BootButton', array(
        				'label'=>'Download',
        				'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        				'size'=>'Large', // '', 'small' or 'large'
        				'url'=>'https://www.google.com/chrome',
    				)); ?>
				</td>
			</tr>
			<tr>
				<td>3</td>
				<td>Sarfari</td>
				<td>
					<?php $this->widget('bootstrap.widgets.BootButton', array(
        				'label'=>'Download',
        				'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        				'size'=>'Large', // '', 'small' or 'large'
        				'url'=>'http://www.apple.com/safari/download/',
    				)); ?>
				</td>
			</tr>
		</tbody>
	</table>
	</section> <!-- row-fluid -->
</section><!-- row-fluid -->