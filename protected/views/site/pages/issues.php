<?php
$this->pageTitle=Yii::app()->name . ' - Issues';
$this->breadcrumbs=array(
	'Issues',
);
?>
<section id="v100" class="row-fluid">
<?php $this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked'=>false, // whether this is a stacked menu
    'items'=>array(
        array('label'=>'History', 'url'=>array('/site/page', 'view'=>'version')),
        array('label'=>'Issues', 'url'=>array('/site/page', 'view'=>'issues'), 'active'=>true),
    ),
)); ?>
<?php echo CHtml::link('Report a Bug', "#modal",  // the link for open the dialog
    array(
        'onclick'=>"{addbugreport();}",
        'class'=>'btn btn-primary',
        'data-toggle'=>'modal'
        ));
?>
</section>
<section id="v100" class="row-fluid">
	<div class="page-header">
		<h1>Bugs and Issues <small>These are the known issues within the application</small></h1>
	</div>
	<h3>Issue List</h3>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Date</th>
				<th>Item</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><strike>1</strike></td>
				<td><strike>07/03/2012</strike></td>
				<td><strike>The main planner popup window is not functioning correctly. It is not transmitting date correctly.</strike></td>
			</tr>
			<tr>
				<td>2</td>
				<td>07/03/2012</td>
				<td>The prayer slot management list is not sorting correctly. It will appear blank when the list is sorted.</td>
			</tr>
			<tr>
				<td>3</td>
				<td>07/03/2012</td>
				<td>The bug reporting popup window redirects incorrectly. The action isn't recognised as an AJAX request.</td>
			</tr>
			<tr>
				<td>4</td>
				<td>07/03/2012</td>
				<td>The icons on forms are appearing above the inputs within Safari and Chrome.</td>
			</tr>
			<tr>
				<td>5</td>
				<td>07/03/2012</td>
				<td>When trying to print the page css styling is interfering with the look and feel. A Print CSS file is required.</td>
			</tr>
			<tr>
				<td>6</td>
				<td>10/03/2012</td>
				<td>After updating a booking record the page will redirect to the current week.</td>
			</tr>
		</tbody>
	</table>
</section><!-- row-fluid -->

<?php $this->beginWidget('bootstrap.widgets.BootModal', array(
    'id'=>'modal',
    'htmlOptions'=>array('class'=>'hide'),
)); ?>
<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h1>Report a Bug</h1>
</div>
<div class="modal-body"></div>
<div class="modal-footer"></div>
<?php $this->endWidget(); ?> 
 
<script type="text/javascript">
function addbugreport() {
	var baseURL = <?php echo CJSON::encode(Yii::app()->createUrl('site/page', array('view'=>'issues'))); ?>;
    <?php 
    	echo CHtml::ajax(
    		array(
            	'url'=>array(
            		'messages/create'
				),
            	'data'=> "js:$(this).serialize()",
            	'type'=>'post',
            	'dataType'=>'json',
            	'success'=>"function(data)
            	{
                	if (data.status == 'failure')
                	{
                    	$('#modal div.modal-body').html(data.div);
                          	// Here is the trick: on submit-> once again this function!
                    	$('#modal div.modal-body form').submit(addBugReport);
                	}
                	else
                	{
                    	$('#modal div.modal-body').html(data.div);
                    	//setTimeout(\"$('#modal').modal('hide') \",3000);
                    	setTimeout(\"window.location.href = baseURL \",1100);
                	}
 
            	} ",
        	)
        )
    ?>;
    return false; 
}
</script>