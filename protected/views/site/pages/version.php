<?php
$this->pageTitle=Yii::app()->name . ' - v1.00a';
$this->breadcrumbs=array(
	'v1.00a',
);
?>
<section id="v100" class="row-fluid">
<?php $this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked'=>false, // whether this is a stacked menu
    'items'=>array(
        array('label'=>'History', 'url'=>array('/site/page', 'view'=>'version'), 'active'=>true),
        array('label'=>'Issues', 'url'=>array('/site/page', 'view'=>'issues')),
    ),
)); ?>
<?php echo CHtml::link('Report a Bug', "#modal",  // the link for open the dialog
    array(
        'onclick'=>"{addbugreport();}",
        'class'=>'btn btn-primary',
        'data-toggle'=>'modal'
        ));
?>
</section>
<section id="v101" class="row-fluid">
	<div class="page-header">
		<h1>Version 1.01a <small>Released Saturday 10th of March 2012</small></h1>
	</div>
	<h3>Description</h3>
	<p class="well">Some changes have been implemented after te first round of user testing.</p>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Item</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>The booking form now contains an option to book several consecutive slots.</td>
			</tr>
			<tr>
				<td>2</td>
				<td>System form have been tweaked.</td>
			</tr>
			<tr>
				<td>3</td>
				<td>The booking form now redirects to the correct week.</td>
			</tr>
			<tr>
				<td>4</td>
				<td>The bug report form has been improved.</td>
			</tr>
			<tr>
				<td>5</td>
				<td>A new booking modal has been implemented to improve the user experience.</td>
			</tr>
		</tbody>
	</table>
</section><!-- row-fluid -->
<section id="v100" class="row-fluid">
	<div class="page-header">
		<h1>Version 1.00a <small>Released Wednesday 7th of March 2012</small></h1>
	</div>
	<h3>Description</h3>
	<p class="well">This update is the first public test. Please use the link in the menu to report bugs and typos.</p>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Item</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>Past prayer slots can no longer be booked.</td>
			</tr>
			<tr>
				<td>2</td>
				<td>Bugs and issues now have a dedicated page.</td>
			</tr>
		</tbody>
	</table>
</section><!-- row-fluid -->
<section id="v085" class="row-fluid">
	<div class="page-header">
		<h1>Version 0.85 <small>Released Saturday 3rd of March 2012</small></h1>
	</div>
	<h3>Description</h3>
	<p class="well">This update has focused on improving the planner controls and style on the data capture interface. Tables and buttons and been implemented to directed the user in how to operate the interface. Colour coding has been chose to help guide with the current state of prayer slots.</p>
	<h3>Improvements</h3>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Item</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>The available pray slot display has been improved with controls and tables.</td>
			</tr>
			<tr>
				<td>2</td>
				<td>The main menu is now fixed to the top of the screen for easy access.</td>
			</tr>
			<tr>
				<td>3</td>
				<td>The private pray slot display has been improved with colour coding and tables.</td>
			</tr>
			<tr>
				<td>4</td>
				<td>The disabled pray slot display has been improved with colour coding.</td>
			</tr>
		</tbody>
	</table>
</section><!-- row-fluid -->
<section id="v085" class="row-fluid">
	<div class="page-header">
		<h1>Version 0.75 <small>Released Thursday 1st of March 2012</small></h1>
	</div>
	<h3>Description</h3>
	<p class="well">This update has focused on improving the looking and feel of the application. The application now displays correctly on mobile and tablet devices. HTML controls and tables have been designed to improve user interaction. Forms have been improved with new icons and AJAX information feeding back to the user.</p>
	<h3>Improvements</h3>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Item</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>A new theme has been designed and built to improve user interaction.</td>
			</tr>
			<tr>
				<td>2</td>
				<td>Several database bugs have been fixed.</td>
			</tr>
			<tr>
				<td>3</td>
				<td>Forms have been redesigned and themed.</td>
			</tr>
		</tbody>
	</table>
</section><!-- row-fluid -->

<?php $this->beginWidget('bootstrap.widgets.BootModal', array(
    'id'=>'modal',
    'htmlOptions'=>array('class'=>'hide'),
)); ?>
<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h1>Report a Bug</h1>
</div>
<div class="modal-body"></div>
<div class="modal-footer"></div>
<?php $this->endWidget(); ?> 
 
<script type="text/javascript">
function addbugreport() {
    var baseURL = <?php echo CJSON::encode(Yii::app()->createUrl('site/page', array('view'=>'version'))); ?>;
    
    <?php echo CHtml::ajax(array(
            'url'=>array('messages/create'),
            'data'=> "js:$(this).serialize()",
            	'type'=>'post',
            	'dataType'=>'json',
            	'success'=>"function(data)
            	{
                	if (data.status == 'failure')
                	{
                    	$('#modal div.modal-body').html(data.div);
                          	// Here is the trick: on submit-> once again this function!
                    	$('#modal div.modal-body form').submit(addBugReport);
                	}
                	else
                	{
                    	$('#modal div.modal-body').html(data.div);
                    	//setTimeout(\"$('#modal').modal('hide') \",3000);
                    	setTimeout(\"window.location.href = baseURL \",1100);
                	}
 
            	} ",
    ))?>;

    return false; 
}
</script>