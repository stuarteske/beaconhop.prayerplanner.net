<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<div class="row-fluid">

<div class="page-header">
	<h1>Login <small>Enter your credentials below to login</small></h1>
</div>

<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
    'id'=>'login-form',
    'type'=>'inline',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
    'htmlOptions'=>array('class'=>'well'),
));
?>

<?php echo $form->errorSummary($model); ?>

<?php 
	$output = '';
	$output .= '<div class="control-group">';
	$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'username') .'</label>';
	$output .= '<div class="controls">';
	$output .= $form->textFieldRow($model, 'username', array('class'=>'span10','maxlength'=>128));
	$output .= '<span class="field-error"><strong>'.$form->error($model,'username').'</strong></span>';
	$output .= '</div>';
	$output .= '</div>';
	echo $output;
?>

<?php 
	$output = '';
	$output .= '<div class="control-group">';
	$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'password') .'</label>';
	$output .= '<div class="controls">';
	$output .= $form->passwordFieldRow($model, 'password', array('class'=>'span10'));
	$output .= '<span class="field-error"><strong>'.$form->error($model,'password').'</strong></span>';
	$output .= '</div>';
	$output .= '</div>';
	echo $output;
?> 

<?php 
	$output = '';
	$output .= '<div class="control-group">';
	$output .= '<div class="controls">';
	$output .= $form->checkboxRow($model, 'rememberMe');
	$output .= '<span class="field-error"><strong>'.$form->error($model,'rememberMe').'</strong></span>';
	$output .= '</div>';
	$output .= '</div>';
	echo $output;
?>

<?php echo CHtml::htmlButton('<i class="icon-ok"></i> Sign In', array('class'=>'btn', 'type'=>'submit')); ?>

<?php $this->endWidget(); ?>

</div><!-- row-fluid -->