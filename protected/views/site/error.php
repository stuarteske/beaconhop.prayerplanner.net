<?php
$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<div class="page-header">
	<h1>Error <small><?php echo $code; ?></small></h1>
</div>

<div class="alert alert-error">
	<a class="close" data-dismiss="alert">×</a>
	<h4 class="alert-heading">Error!</h4>
	<?php echo CHtml::encode($message); ?>
</div>