<?php
$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contact',
);
?>

<div class="row-fluid">

<div class="page-header">
	<h1>Contact Us <small>If you have any questions please contact us</small></h1>
</div>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="alert alert-success">
	<a class="close" data-dismiss="alert">×</a>
	<h4 class="alert-heading">Success</h4>
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
    'id'=>'contact-form',
    'type'=>'inline',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
    'htmlOptions'=>array('class'=>'well'),
)); ?>

	<?php echo $form->errorSummary($model); ?>
	
<?php 
	$output = '';
	$output .= '<div class="control-group">';
	$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'name') .'</label>';
	$output .= '<div class="controls">';
	$output .= $form->textFieldRow($model, 'name', array('class'=>'span10','maxlength'=>64));
	$output .= '<span class="field-error"><strong>'.$form->error($model,'name').'</strong></span>';
	$output .= '</div>';
	$output .= '</div>';
	echo $output;
?>

<?php 
	$output = '';
	$output .= '<div class="control-group">';
	$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'email') .'</label>';
	$output .= '<div class="controls">';
	$output .= $form->textFieldRow($model, 'email', array('class'=>'span10','maxlength'=>128));
	$output .= '<span class="field-error"><strong>'.$form->error($model,'email').'</strong></span>';
	$output .= '</div>';
	$output .= '</div>';
	echo $output;
?>
	
<?php 
	$output = '';
	$output .= '<div class="control-group">';
	$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'subject') .'</label>';
	$output .= '<div class="controls">';
	$output .= $form->textFieldRow($model, 'subject', array('class'=>'span10','maxlength'=>128));
	$output .= '<span class="field-error"><strong>'.$form->error($model,'subject').'</strong></span>';
	$output .= '</div>';
	$output .= '</div>';
	echo $output;
?>

<?php 
	$output = '';
	$output .= '<div class="control-group">';
	$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'body') .'</label>';
	$output .= '<div class="controls">';
	$output .= $form->textAreaRow($model, 'body', array('class'=>'span10', 'rows'=>5));
	$output .= '<span class="field-error"><strong>'.$form->error($model,'body').'</strong></span>';
	$output .= '</div>';
	$output .= '</div>';
	echo $output;
?>

<?php if(CCaptcha::checkRequirements()): ?>
	<div>
	<?php $this->widget('CCaptcha'); ?>
	</div>
		
	<?php 
		$output = '';
		$output .= '<div class="control-group">';
		$output .= '<label class="control-label" for="inputIcon">'. $form->labelEx($model,'verifyCode') .'</label>';
		$output .= '<div class="controls">';
		$output .= $form->textFieldRow($model, 'verifyCode', array('class'=>'span10'));
		$output .= '<span class="field-error"><strong>'.$form->error($model,'name').'</strong></span>';
		$output .= '</div>';
		$output .= '</div>';
		echo $output;
	?>
		
	<div class="alert alert-info">
		<a class="close" data-dismiss="alert">×</a>
		<strong>Captcha</strong> Please enter the letters as they are shown in the image above. Letters are not case-sensitive.
	</div>
<?php endif; ?>

	<?php echo CHtml::htmlButton('<i class="icon-ok"></i> Submit', array('class'=>'btn', 'type'=>'submit')); ?>

<?php $this->endWidget(); ?>

<?php endif; ?>

</div><!--row-fluid-->