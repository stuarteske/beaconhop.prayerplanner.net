<?php
return array (
  'template' => 'default',
  'tablePrefix' => 'pb_',
  'modelPath' => 'application.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
);
