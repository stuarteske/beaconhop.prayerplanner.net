-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 21, 2012 at 11:11 PM
-- Server version: 5.5.9
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `prayerplanner`
--

-- --------------------------------------------------------

--
-- Table structure for table `AuthAssignment`
--

DROP TABLE IF EXISTS `AuthAssignment`;
CREATE TABLE IF NOT EXISTS `AuthAssignment` (
  `itemname` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `userid` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `bizrule` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `AuthAssignment`
--

INSERT INTO `AuthAssignment` VALUES('Administrator', '3', NULL, 'N;');
INSERT INTO `AuthAssignment` VALUES('RBAC Manager', '3', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `AuthItem`
--

DROP TABLE IF EXISTS `AuthItem`;
CREATE TABLE IF NOT EXISTS `AuthItem` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `bizrule` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `AuthItem`
--

INSERT INTO `AuthItem` VALUES('Administrator', 2, 'A site administrator.', '', 's:0:"";');
INSERT INTO `AuthItem` VALUES('Auth Assignments Manager', 2, 'Manages Role Assignments. RBAM required role.', NULL, 'N;');
INSERT INTO `AuthItem` VALUES('Auth Items Manager', 2, 'Manages Auth Items. RBAM required role.', NULL, 'N;');
INSERT INTO `AuthItem` VALUES('Authenticated', 2, 'Default role for users that are logged in. RBAC default role.', 'return !Yii::app()->getUser()->getIsGuest();', 'N;');
INSERT INTO `AuthItem` VALUES('createBooking', 0, '(C)RUD. Create Bookings.', NULL, 'N;');
INSERT INTO `AuthItem` VALUES('createUser', 0, '(C)RUD, create users.', NULL, 'N;');
INSERT INTO `AuthItem` VALUES('deleteBooking', 0, 'CRU(D). Delete Bookings.', NULL, 'N;');
INSERT INTO `AuthItem` VALUES('deleteUser', 0, 'CRU(D). Delete Users.', NULL, 'N;');
INSERT INTO `AuthItem` VALUES('Guest', 2, 'Default role for users that are not logged in. RBAC default role.', 'return Yii::app()->getUser()->getIsGuest();', 'N;');
INSERT INTO `AuthItem` VALUES('RBAC Manager', 2, 'Manages Auth Items and Role Assignments. RBAM required role.', NULL, 'N;');
INSERT INTO `AuthItem` VALUES('readBooking', 0, 'C(R)UD. Read Bookings.', NULL, 'N;');
INSERT INTO `AuthItem` VALUES('readUser', 0, 'C(R)UD. Read Users.', NULL, 'N;');
INSERT INTO `AuthItem` VALUES('updateBooking', 0, 'CR(U)D. Update Bookings.', NULL, 'N;');
INSERT INTO `AuthItem` VALUES('updateUser', 0, 'CR(U)D. Updates Users.', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `AuthItemChild`
--

DROP TABLE IF EXISTS `AuthItemChild`;
CREATE TABLE IF NOT EXISTS `AuthItemChild` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `AuthItemChild`
--

INSERT INTO `AuthItemChild` VALUES('RBAC Manager', 'Administrator');
INSERT INTO `AuthItemChild` VALUES('RBAC Manager', 'Auth Assignments Manager');
INSERT INTO `AuthItemChild` VALUES('RBAC Manager', 'Auth Items Manager');
INSERT INTO `AuthItemChild` VALUES('Administrator', 'Authenticated');
INSERT INTO `AuthItemChild` VALUES('Guest', 'createBooking');
INSERT INTO `AuthItemChild` VALUES('Administrator', 'createUser');
INSERT INTO `AuthItemChild` VALUES('Administrator', 'deleteBooking');
INSERT INTO `AuthItemChild` VALUES('Administrator', 'deleteUser');
INSERT INTO `AuthItemChild` VALUES('Authenticated', 'Guest');
INSERT INTO `AuthItemChild` VALUES('Guest', 'readBooking');
INSERT INTO `AuthItemChild` VALUES('Administrator', 'readUser');
INSERT INTO `AuthItemChild` VALUES('Administrator', 'updateBooking');
INSERT INTO `AuthItemChild` VALUES('Administrator', 'updateUser');

-- --------------------------------------------------------

--
-- Table structure for table `pb_bookings`
--

DROP TABLE IF EXISTS `pb_bookings`;
CREATE TABLE IF NOT EXISTS `pb_bookings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `booked_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `users_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `edited_user_id` int(10) unsigned DEFAULT NULL,
  `time_created` datetime NOT NULL,
  `time_edited` datetime DEFAULT NULL,
  `slot_booked` datetime NOT NULL,
  `slot_locked` int(1) NOT NULL DEFAULT '0',
  `slot_disabled` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `created_user_id` (`booked_user_id`,`edited_user_id`,`slot_booked`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=75 ;

--
-- Dumping data for table `pb_bookings`
--


-- --------------------------------------------------------

--
-- Table structure for table `pb_messages`
--

DROP TABLE IF EXISTS `pb_messages`;
CREATE TABLE IF NOT EXISTS `pb_messages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `msg` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pb_messages`
--


-- --------------------------------------------------------

--
-- Table structure for table `pb_settings`
--

DROP TABLE IF EXISTS `pb_settings`;
CREATE TABLE IF NOT EXISTS `pb_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item` (`item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pb_settings`
--


-- --------------------------------------------------------

--
-- Table structure for table `pb_user`
--

DROP TABLE IF EXISTS `pb_user`;
CREATE TABLE IF NOT EXISTS `pb_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(10) unsigned DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(10) unsigned DEFAULT NULL,
  `system_admin` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `pb_user`
--

INSERT INTO `pb_user` VALUES(1, 'admin@prayerplanner.net', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', '2012-06-21 23:01:03', '2012-01-20 15:10:05', 0, '2012-01-20 15:42:54', 0, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `AuthAssignment`
--
ALTER TABLE `AuthAssignment`
  ADD CONSTRAINT `AuthAssignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `AuthItemChild`
--
ALTER TABLE `AuthItemChild`
  ADD CONSTRAINT `AuthItemChild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `AuthItemChild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;
