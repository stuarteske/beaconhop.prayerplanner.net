<?php

class GreetingController extends Controller
{
	public $message = '';
	
	public function actionIndex()
	{
		$DB_Message = Messages::model()->findByPk(1);
		$this->message = $DB_Message->msg;
		$this->render('index', array('content' => $this->message));
	}

	public function actionIndextwo()
	{
		$DB_Message = Messages::model()->findByPk(2);
		$this->message = $DB_Message->msg;
		$this->render('indextwo', array('content' => $this->message));
	}
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}