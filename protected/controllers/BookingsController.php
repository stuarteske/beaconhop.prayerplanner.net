<?php

class BookingsController extends Controller
{
	//const _timeWeekStarts = 'Sunday';	// The Calendar starts on day
	const TIME_DAYS_OFFSET = 4;			// Time offset to have the week begin on Sunday. Timestamp starts on a Wednesday
	//const _timeZone = 'Europe/London';	// Not currently used
	
	private $bookings_week_id = 1;
	private $parsed_booking_entries = array();
	
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$userModel=new User;
		
		return array(
			array(
				'allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('create','planner'),
                'users'=>array('*'),
			),
			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','update','delete','index','view'),
            	'users'=>$userModel->getAdminUsers(),
        	),
			array(
				'deny',  // deny all users
            	'users'=>array('*'),
			), 
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{	
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$invalid_flag = false;
		if ( (isset($_GET['wid'])) && (isset($_GET['hid'])) ) {
			if (is_numeric($_GET['wid'])) {
				$wid = intval($_GET['wid']);
			} else {
				$invalid_flag = true;
			}
		
			if (is_numeric($_GET['hid'])) {
				$hid = intval($_GET['hid']);
			} else {
				$invalid_flag = true;
			}
		} else {
			$invalid_flag = true;
		}
		if ($invalid_flag) throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		
		$dataProvider=new CActiveDataProvider(
			'Bookings',
			array(
				'criteria'=>array(
        			'condition'=>'slot_booked=:specifieddattime',
        			'order'=>'id DESC',
        			'params'=>array(
						':specifieddattime'=>$this->getSQLDateFromWIDandDID($wid, $hid),
					),
    			),
    			'pagination'=>array(
        			'pageSize'=>10,
    			),
    		)
		);
	
		$model=new Bookings;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		$consecutive_hours_to_book = Yii::app()->request->getParam('block_booking', 1);
		
		if(isset($_POST['Bookings'])) {
			
			for($i=0;$i<$consecutive_hours_to_book;$i++) {
				$model=new Bookings;
				$_POST['Bookings']['slot_booked'] = $this->getSQLDateFromWIDandDID($wid,$hid + $i);
				$model->attributes=$_POST['Bookings'];
				$saved_flag = $model->save();
			}
			
			if($saved_flag) {
				if (Yii::app()->request->isAjaxRequest) {
                    echo CJSON::encode(array(
                        'status'=>'success', 
                        'div'=>'<div class="alert alert-success">
									<strong>Booking Successful</strong>
								</div>',
                        ));
                    exit;               
                } else $this->redirect(array('bookings/planner', 'wid'=>$wid));
			}
		}

		if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(array(
                'status'=>'failure', 
                'div'=>$this->renderPartial(
                	'_form',
                	array(
                		'model'=>$model,
						'dataProvider'=>$dataProvider,
						'wid'=>$wid,
						'hid'=>$hid,
                	),
                	true
                )
            ));
            exit;               
        } else $this->render('create',array(
			'model'=>$model,
			'dataProvider'=>$dataProvider,
			'wid'=>$wid,
			'hid'=>$hid,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Bookings']))
		{
			$model->attributes=$_POST['Bookings'];
			if($model->save())
				$this->redirect(array('planner'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{

        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Bookings');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Bookings('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Bookings']))
			$model->attributes=$_GET['Bookings'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Bookings::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bookings-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	/********* Custom Functions *********/
	
	/**
	 * Displays a particular model.
	 */
	public function actionPlanner() {
		//$this->$layout='//layouts/column1';
		
		// Setup a new bookings model
		$model=new Bookings;
		$userModel=new User;
	
		// Generate The current timestamp
		$_currentTimestamp = time();
		
		// Identified the specified week
		if( isset($_GET['wid']) && (is_numeric($_GET['wid'])) ) {
			// Store the specified week
			$this->bookings_week_id = intval($_GET['wid']);
		} else {
			// Find the current week number
			$this->bookings_week_id = floor( ($_currentTimestamp + (self::TIME_DAYS_OFFSET * 60 * 60 * 24)) / (60 * 60 * 24 * 7) ) - 1;
		}
		
		$this->parseBookingEntriesForThisWeek();
		
		// Render the page
		$this->render(
			'planner', 
			array(
				'currentStamp'=>$_currentTimestamp,
				'plannerColumnTitle'=>$model->getColumnTitles(),
				'plannerRowTitle'=>$model->gettRowTitles(),
				'specifiedWeek'=>$this->bookings_week_id,
				'specifiedWeekReadableDate'=>$this->getWeekReadableDateRange($this->bookings_week_id),
				'parseBookingEntriesForThisWeek'=>$this->parsed_booking_entries,
				'isAdminUser'=>$userModel->isAdminUser(Yii::app()->user->id),
			)
		);
	}
	
	/** 
	 * Generate the link to navigate to the next week.
	 */
	public function getNextWeekLink() {
		return $this->bookings_week_id + 1;
	}
	
	/**
	 * Generate the link to navigate to the previous week.
	 */
	public function getPreviousWeekLink() {
		return $this->bookings_week_id - 1;
	}
	
	/**
	 * Generate the readable week label of the date range.
	 */
	public function getWeekReadableDateRange() {
		$fromDate = date( 'l, jS F Y', mktime(0,0,0,1,(($this->bookings_week_id * 7) + self::TIME_DAYS_OFFSET),1970) );
		$toDate = date( 'l, jS F Y', mktime(0,0,0,1,((($this->bookings_week_id * 7) + 6) + self::TIME_DAYS_OFFSET),1970) );
		return $fromDate . ' to ' . $toDate;
	}
	
	/**
	 * Generate the readable date of the targeted day.
	 * @param integer $did the ID of the day to be targeted
	 */
	public function getDayReadableDateInfo($did) {
		return date( 'jS F', mktime(0,0,0,1,((($this->bookings_week_id * 7) + $did ) + self::TIME_DAYS_OFFSET),1970) );
	}
	
	/**
	 * Generate the readable date from the week id and the hour id.
	 * @param integer $wid the ID of the targeted week
	 * @param integer $hid the ID of the targeted hour
	 */
	public function getReadableDateFromWIDandDID($wid, $hid) {
		return date( 'g:ia l, jS F Y', mktime($hid,0,0,1,($wid * 7) + self::TIME_DAYS_OFFSET,1970));
	}
	
	/**
	 * Generate the MySQL date from the week id and the hour id.
	 * @param integer $wid the ID of the targeted week
	 * @param integer $hid the ID of the targeted hour
	 */
	public function getSQLDateFromWIDandDID($wid, $hid) {
		return date( 'Y-m-d H:00:00', mktime($hid,0,0,1,($wid * 7) + self::TIME_DAYS_OFFSET,1970));
	}
	
	/**
	 * Generate the timestamp from the week id and the hour id.
	 * @param integer $wid the ID of the targeted week
	 * @param integer $hid the ID of the targeted hour
	 * @return integer timestamp
	 */
	public function getTimetampFromWIDandDID($wid, $hid) {
		return mktime($hid,0,0,1,($wid * 7) + self::TIME_DAYS_OFFSET,1970);
	}
	
	public function getHourNumberFromHourAndDayID( $hour, $day ) {
		$hour_number = ($hour - 1) + (($day - 1) * 24);
		return $hour_number;
	}
	
	/**
	 * Retrieve the complete list of bookings for te current week.
	 */
	private function getBookingEntriesForThisWeek() {
		// MySQL Datetime format : YYYY-MM-DD HH:MM:SS
		// Earlest entry : 2012-01-20 12:00:00
		$dateRangeLowerLimit = date( 'Y-m-d 00:00:00', mktime(0,0,0,1,(($this->bookings_week_id * 7) + self::TIME_DAYS_OFFSET),1970) );
		$dateRangeUpperLimit = date( 'Y-m-d 23:00:00', mktime(0,0,0,1,((($this->bookings_week_id * 7) + 6) + self::TIME_DAYS_OFFSET),1970) );
		
		$booking_entries=Bookings::model()->findAll(array(
			'select'=>'*',
			'condition'=>'slot_booked>=:lowerLimit AND slot_booked<=:upperLimit',
			'params'=>array(
				':lowerLimit'=>$dateRangeLowerLimit,
				':upperLimit'=>$dateRangeUpperLimit,
			),
		));
		
		return $booking_entries;
	}
	
	/**
	 * Parse the booking list.
	 */
	private function parseBookingEntriesForThisWeek() {
		$booking_entries=$this->getBookingEntriesForThisWeek();
		$parsed_booking_entries=array();
		
		if (! empty($booking_entries)) {
			$hours=0;
			while ($hours < (24*7)) {
				$target_date = date( 'Y-m-d H:00:00', mktime($hours,0,0,1,(($this->bookings_week_id * 7) + self::TIME_DAYS_OFFSET),1970) );
				$parsed_booking_entries[$hours] = array();
				
				for ($i=0;$i<count($booking_entries);$i++) {
					if ($target_date == $booking_entries[$i]->slot_booked) {
						array_push($parsed_booking_entries[$hours], $booking_entries[$i]);
					} else {
						//$parsed_booking_entries[$hours][$i] = $target_date;
					}
				}
				
				$hours++;
			}
		
		$this->parsed_booking_entries = $parsed_booking_entries;
		}
		
	}
	
	/**
	 * Get the names of users who have booked and add the to spans.
	 * @param integer $hid the ID of the targete hour within the week
	 * @param integer $limit the number of names to be returned
	 * @return string HMTL snaps containing the booked user names
	 */
	public function getBookingHTMLContentForHour($hid,$limit=4) {
		$output = '';
		$output .= '<table class="table table-condensed">';
		$output .= '<tbody>';
		
		for ($i=0;$i<$limit;$i++) {
			$output .= '<tr>';
			$output .= (!empty($this->parsed_booking_entries[$hid][$i]->users_name)) ? '<td style="text-align:center;"><i class="icon-user"></i></td>' : '<td></td>';
			$output .= (!empty($this->parsed_booking_entries[$hid][$i]->users_name)) ? '<td>'.$this->parsed_booking_entries[$hid][$i]->users_name.'</td>' : '<td>&nbsp;</td>';
			$output .= '</tr>';
		}
		
		//if (!empty($this->parsed_booking_entries[$hid])) {
		//	for ($i=0;$i<count($this->parsed_booking_entries[$hid]);$i++) {
		//		if ($i<$limit) $output .= '<span class="quiet">'. $this->parsed_booking_entries[$hid][$i]->users_name . '</span><br />';
		//	}
		//}
		$output .= '</tbody>';
		$output .= '</table>';
		return $output;
	}
	
	/**
	 * Return the css class of the booking button status.
	 * @param integer $hid the ID of the targete hour within the week
	 * @return string CSS class defining the status of the button
	 */
	public function getBookingCSSStatusForHour($hid) {
		$disabled_flag = false;
		$locked_flag = false;
		
		if (!empty($this->parsed_booking_entries[$hid])) {
			for ($i=0;$i<count($this->parsed_booking_entries[$hid]);$i++) {
				
				if ($this->parsed_booking_entries[$hid][$i]->slot_locked)
					$locked_flag = true;
				if ($this->parsed_booking_entries[$hid][$i]->slot_disabled)
					$disabled_flag = true;
			
			}
		}
		
		$output = 'button01';
		if ($disabled_flag) $output='button03';
			else if ($locked_flag) $output='button02';
		
		return $output;
	}
	
}
