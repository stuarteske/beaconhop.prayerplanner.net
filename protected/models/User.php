<?php

/**
 * This is the model class for table "pb_user".
 *
 * The followings are the available columns in table 'pb_user':
 * @property string $id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $last_login_time
 * @property string $create_time
 * @property string $create_user_id
 * @property string $update_time
 * @property string $update_user_id
 * @property string $system_admin
 *
 * The followings are the available model relations:
 * @property Bookings[] $bookings
 */
class User extends CActiveRecord
{
	const ADMIN_NO = 0;
	const ADMIN_YES = 1;
	
	public $password_repeat;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pb_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, name', 'required'),
			array('username', 'email'),
			array('username, name', 'unique'),
			array('username', 'length', 'max'=>128),
			array('password, password_repeat', 'length', 'max'=>256),
			array('password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>'The password fields do not match.'),
			array('name', 'length', 'max'=>64),
			array('create_user_id, update_user_id', 'length', 'max'=>10),
			array('last_login_time, create_time, update_time, password_repeat', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, name, last_login_time, create_time', 'safe', 'on'=>'search'),
			// Purify
			array('username','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('password','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('name','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('password_repeat','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('create_user_id','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('update_user_id','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('last_login_time','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('create_time','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('update_time','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// Strip Tags
			/*array('username','filter',function stripthosetags($value) { return strip_tags($value); }),
			array('password','filter','filter'=>function($v){ return strip_tags($v);}),
			array('name','filter','filter'=>function($v){ return strip_tags($v);}),
			array('password_repeat','filter','filter'=>function($v){ return strip_tags($v);}),
			array('create_user_id','filter','filter'=>function($v){ return strip_tags($v);}),
			array('create_time','filter','filter'=>function($v){ return strip_tags($v);}),
			array('update_user_id','filter','filter'=>function($v){ return strip_tags($v);}),
			array('last_login_time','filter','filter'=>function($v){ return strip_tags($v);}),
			array('update_time','filter','filter'=>function($v){ return strip_tags($v);}),*/
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bookings' => array(self::HAS_MANY, 'Bookings', 'booked_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Email',
			'password' => 'Password',
			'name' => 'Name',
			'last_login_time' => 'Last Login Time',
			'create_time' => 'Create Time',
			'create_user_id' => 'Create User',
			'update_time' => 'Update Time',
			'update_user_id' => 'Update User',
			'system_admin' => 'System Admin',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('last_login_time',$this->last_login_time,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('create_user_id',$this->create_user_id,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('update_user_id',$this->update_user_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	//*********** Custom Functions ***********//
	
	/**
	 * Prepares create_time, update_time, create_user_id, update_user_id attributes before performing validation.
	 */
	protected function beforeValidate() {
		if($this->isNewRecord) {
			// set the create date, last updated date and the user doing the creating
			$this->create_time = $this->update_time = new CDbExpression( 'NOW()' );
			$this->create_user_id = $this->update_user_id = Yii::app()->user->id;
		} else {
			//not a new record, so just set the last updated time and last updated user id
			$this->update_time = new CDbExpression( 'NOW()' );
			$this->update_user_id = Yii::app()->user->id;
       }
       return parent::beforeValidate();
	}
	
	/**
	 * perform one-way encryption on the password before we store it in the database
	 */
	protected function afterValidate() {
		parent::afterValidate();
		$this->password = $this->encrypt($this->password);
	}
     
	public function encrypt($value) {
		return md5($value);
	}
	
	//*********** Custom Functions ***********//
	
	/**
	 * @return array the admin options for the form drop down
	 */
	public function getAdminOptions() {
		return array(
			self::ADMIN_NO => 'No',
			self::ADMIN_YES => 'Yes',
		); 
	}
	
	/**
	 * @return string the admin text display for the user form view
	 */
	public function getAdminText() {
		$adminOptions=$this->adminOptions;
		return isset($adminOptions[$this->system_admin]) ?
			$adminOptions[$this->system_admin] : "unknown status ({$this->system_admin})";
	}
	
	/**
	 * @return array the of admin UID's
	 */
	public function getAdminUsers() {
		$user_records=User::model()->findAll(array(
			'select'=>'username',
			'condition'=>'system_admin=:admin',
			'params'=>array(
				':admin'=>1
			),
		));
		
		$user_array = array();
		foreach ($user_records as $key => $user_record) {
			array_push($user_array, $user_record->username);
		}
		return $user_array;
	}
	
	/**
	 * @return array the of admin UID's
	 */
	public function isAdminUser($uid = 0) {
		return User::model()->exists(array(
			'condition'=>'id =:uid AND system_admin=:admin',
			'params'=>array(
				':uid'=>$uid,
				':admin'=>1,
			),
		));
	}
}