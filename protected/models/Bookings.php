<?php

/**
 * This is the model class for table "pb_bookings".
 *
 * The followings are the available columns in table 'pb_bookings':
 * @property string $id
 * @property string $booked_user_id
 * @property string $users_name
 * @property string $username
 * @property string $edited_user_id
 * @property string $time_created
 * @property string $time_edited
 * @property string $slot_booked
 * @property integer $slot_locked
 * @property integer $slot_disabled
 */
class Bookings extends CActiveRecord
{
	const LOCKED_NO = 0;
	const LOCKED_YES = 1;
	const DISABLED_NO = 0;
	const DISABLED_YES = 1;
	
	public $blockBookingValues = array(
		1=>'1',
		2=>'2',
		3=>'3',
		4=>'4',
		5=>'5',
		6=>'6',
		7=>'7',
		8=>'8',
		9=>'9',
		10=>'10',
		11=>'11',
		12=>'12',
		13=>'13',
		14=>'14',
		15=>'15',
		16=>'16',
		17=>'17',
		18=>'18',
		19=>'19',
		20=>'20',
		21=>'21',
		22=>'22',
		23=>'23',
		24=>'24'
	);
	
	public $bookingDayTitles = array(
		1=>'Sunday',
		2=>'Monday',
		3=>'Tuesday',
		4=>'Wednesday',
		5=>'Thursday',
		6=>'Friday',
		7=>'Saturday'
	);
	
	public $bookingRowTitles = array(
		'Time',
		'00:00',
		'01:00',
		'02:00',
		'03:00',
		'04:00',
		'05:00',
		'06:00',
		'07:00',
		'08:00',
		'09:00',
		'10:00',
		'11:00',
		'12:00',
		'13:00',
		'14:00',
		'15:00',
		'16:00',
		'17:00',
		'18:00',
		'19:00',
		'20:00',
		'21:00',
		'22:00',
		'23:00',
		'Time'
	);
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Bookings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pb_bookings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('users_name, username, time_created, slot_booked', 'required'),
			array('username', 'email'),
			array('booked_user_id, slot_locked, slot_disabled', 'numerical', 'integerOnly'=>true),
			array('booked_user_id, edited_user_id', 'length', 'max'=>10),
			array('users_name', 'length', 'max'=>64),
			array('username', 'length', 'max'=>128),
			array('time_edited', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, booked_user_id, users_name, username, edited_user_id, time_created, time_edited, slot_booked, slot_locked, slot_disabled', 'safe', 'on'=>'search'),
			
			array('users_name','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('username','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('slot_booked','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('slot_locked','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('slot_disabled','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			
			/*array('users_name','filter','filter'=>function($v){ return strip_tags($v);}),
			array('username','filter','filter'=>function($v){ return strip_tags($v);}),
			array('slot_booked','filter','filter'=>function($v){ return strip_tags($v);}),
			array('slot_locked','filter','filter'=>function($v){ return strip_tags($v);}),
			array('slot_disabled','filter','filter'=>function($v){ return strip_tags($v);}),*/
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'booked_user_id' => 'Booked User',
			'users_name' => 'Name',
			'username' => 'Email',
			'edited_user_id' => 'Edited User',
			'time_created' => 'Time Created',
			'time_edited' => 'Time Edited',
			'slot_booked' => 'Desired Prayer Slot',
			'slot_locked' => 'Private Prayer Slot',
			'slot_disabled' => 'Disable this Prayer Slot',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('booked_user_id',$this->booked_user_id,true);
		$criteria->compare('users_name',$this->users_name,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('edited_user_id',$this->edited_user_id,true);
		$criteria->compare('time_created',$this->time_created,true);
		$criteria->compare('time_edited',$this->time_edited,true);
		$criteria->compare('slot_booked',$this->slot_booked,true);
		$criteria->compare('slot_locked',$this->slot_locked);
		$criteria->compare('slot_disabled',$this->slot_disabled);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
    public function scopes() {
        return array(
            'published'=>array(
                'condition'=>'status=1',
            ),
            'recently'=>array(
                'order'=>'create_time DESC',
                'limit'=>5,
            ),
        );
    }
	
	//*********** Custom Functions ***********//
	
	/**
	 * Prepares time_created, create_user_id, time_edited and update_user_id attributes before performing validation.
	 */
	protected function beforeValidate() {
		if($this->isNewRecord) {
			// set the create date, last updated date and the user doing the creating
			$this->time_created = $this->time_edited = new CDbExpression( 'NOW()' );
			$this->booked_user_id = $this->edited_user_id = Yii::app()->user->id;
		} else {
			//not a new record, so just set the last updated time and last updated user id
			$this->time_edited = new CDbExpression( 'NOW()' );
			$this->edited_user_id = Yii::app()->user->id;
       }
       return parent::beforeValidate();
	}
	
	/**
	 * @return array the block booking possible values
	 */
	public function getBlockBookingValues() {
		return $this->blockBookingValues;
	}
	
	/**
	 * @return array the locked options for the form drop down
	 */
	public function getLockedOptions() {
		return array(
			self::LOCKED_NO => 'No',
			self::LOCKED_YES => 'Yes',
		); 
	}
	
	/**
	 * @return array the disabled options for the form drop down
	 */
	public function getDisabledOptions() {
		return array(
			self::DISABLED_NO => 'No',
			self::DISABLED_YES => 'Yes',
		); 
	}
	
	/**
	 * @return string the locked text display for the booking view
	 */
	public function getLockedText() {
		$lockedOptions=$this->lockedOptions;
		return isset($lockedOptions[$this->slot_locked]) ?
			$lockedOptions[$this->slot_locked] : "unknown status ({$this->slot_locked})";
	}
	
	/**
	 * @return string the disabled text display for the booking view
	 */
     public function getDisabledText() {
		$disabledOptions=$this->disabledOptions;
		return isset($disabledOptions[$this->slot_disabled]) ?
			$disabledOptions[$this->slot_disabled] : "unknown type ({$this->slot_disabled})";
	}
	
	/**
	 * @return array the planner column titles
	 */
	public function getColumnTitles() {
		return $this->bookingDayTitles;
	}
	
	/**
	 * @return array the planner row titles
	 */
	public function gettRowTitles() {
		return $this->bookingRowTitles; 
	}
}