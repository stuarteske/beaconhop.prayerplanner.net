<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
	public $name;
	public $email;
	public $subject;
	public $body;
	public $verifyCode;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('name, email, subject, body', 'required'),
			// email has to be a valid email address
			array('email', 'email'),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
			array('name','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('email','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('subject','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('body','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('verifyCode','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			
			/*array('name','filter','filter'=>function($v){ return strip_tags($v);}),
			array('email','filter','filter'=>function($v){ return strip_tags($v);}),
			array('subject','filter','filter'=>function($v){ return strip_tags($v);}),
			array('body','filter','filter'=>function($v){ return strip_tags($v);}),
			array('verifyCode','filter','filter'=>function($v){ return strip_tags($v);}),*/
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'verifyCode'=>'Verification Code',
		);
	}
}