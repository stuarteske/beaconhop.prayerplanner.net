<?php

if ($_SERVER["HTTP_HOST"] === 'localhost') {
    $connectionString = 'mysql:host=localhost;dbname=prayerplanner';
    $dbusername = 'prayerplanner';
    $dbpassword = 'prayerplanner321';
} else {
    $connectionString = 'mysql:host=localhost;dbname=shapeshi_beaconhop';
    $dbusername = 'shapeshi_beaconh';
    $dbpassword = 'beaconhopdb321';
}

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',
	// application components
	'components'=>array(
//		'db'=>array(
//			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
//		),
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString' => $connectionString,
			'emulatePrepare' => true,
			'username' => $dbusername,
			'password' => $dbpassword,
			'charset' => 'utf8',
		),
	),
);