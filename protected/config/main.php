<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// Generate the database login details
if ($_SERVER["HTTP_HOST"] === 'localhost') {  
	$connectionString = 'mysql:host=localhost;dbname=prayerplanner';
	$dbusername = 'prayerplanner';
	$dbpassword = 'prayerplanner321';
} else {
	$connectionString = 'mysql:host=localhost;dbname=shapeshi_beaconhop';
	$dbusername = 'shapeshi_beaconh';
	$dbpassword = 'beaconhopdb321';
}

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Prayer Planner',
	'timeZone'=>'Europe/London', 
	//'theme'=>'prayerbooker',
	
	// preloading 'log' component
	'preload'=>array(
		'log',
		'bootstrap'
	),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.widgets.bootstrap.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		/*
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'password',
		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
			'generatorPaths'=>array(
            	'bootstrap.gii', // since 0.9.1
        	),
		),
		*/
		'rbam'=>array(
    		// RBAM Configuration
    		'initialise'=>null,
  		),
	),

	// application components
	'components'=>array(
		'clientScript'=>array(
        	//'packages'=>array(
            //    'jquery'=>array(
            //        'baseUrl'=>Yii::app()->request->baseUrl . '/js',
            //        'js'=>array('jquery.1.7.2.min.js')
            //    )
            //)
            'scriptMap'=>array(
				'jquery.js'=>false,
				'jquery.min.js'=>false,
			),
        ),
        // Security settings, URL - http://www.yiiframework.com/doc/guide/1.1/en/topics.security
		'request'=>array(
            //'enableCsrfValidation'=>true, // Cross-site Request Forgery Prevention
            'enableCookieValidation'=>true, // Cookie Attack Prevention
        ),
		'authManager'=>array(
			'class'=>'CDbAuthManager',
			'connectionID'=>'db',
		),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>array(
                '' => 'bookings/planner',
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>'
            ),
        ),
		'bootstrap'=>array(
        	'class'=>'ext.bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
        	'coreCss'=>true, // whether to register the Bootstrap core CSS (bootstrap.min.css), defaults to true
        	'responsiveCss'=>false, // whether to register the Bootstrap responsive CSS (bootstrap-responsive.min.css), default to false
        	'plugins'=>array(
            	// Optionally you can configure the "global" plugins (button, popover, tooltip and transition)
            	// To prevent a plugin from being loaded set it to false as demonstrated below
            	'transition'=>true,
            	'tooltip'=>array(
                	'selector'=>'a.tooltip', // bind the plugin tooltip to anchor tags with the 'tooltip' class
                	'options'=>array(
                    	'placement'=>'bottom', // place the tooltips below instead
                	),
            	),
        	),
    	),
		'db'=>array(
			'connectionString' => $connectionString,
			'emulatePrepare' => true,
			'username' => $dbusername,
			'password' => $dbpassword,
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
           	array(
            	'class'=>'CFileLogRoute',
            	'levels'=>'error',
            	'logFile'=>'ErrorMessages.log',
        	),
        	array(
            	'class'=>'CFileLogRoute',
            	'levels'=>'warning',
            	'logFile'=>'WarningMessages.log',
        	),
        	array(
            	'class'=>'CFileLogRoute',
            	'levels'=>'info, trace',
            	'logFile'=>'infoMessages.log',
			), 
			array(
				'class'=>'CWebLogRoute',
            	'levels'=>'warning, error, info, trace',
        	),
        	),
		),
		'cache'=>array(
            'class'=>'system.caching.CFileCache',
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'info@stuarteske.com',
	),
);