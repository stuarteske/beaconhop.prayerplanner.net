<?php

// change the following paths if necessary
if ($_SERVER["HTTP_HOST"] === 'localhost') {
    $yiic=dirname(__FILE__).'/../../yii/framework/yiic.php';
} else {
    $yiic=dirname(__FILE__).'/../../../../yii/framework/yiic.php';
}

$config=dirname(__FILE__).'/config/console.php';

require_once($yiic);
